package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.repository.BookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @Test
    public void findById_whenExistingId_thenBookShouldBeFound() {
        Book book = Book.builder().id(1L).build();

        when(bookRepository.findById(eq(1L))).thenReturn(Optional.of(book));
        Book found = bookService.findById(1L);

        assertThat(found).isEqualTo(book);
    }

    @Test
    public void findById_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        when(bookRepository.findById(eq(1L))).thenThrow(new NotFoundException());

        assertThatThrownBy(() -> bookService.findById(1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void create_whenNewBook_thenSaveShouldBeCalled() {
        Book book = Book.builder().build();

        bookService.create(book);

        verify(bookRepository).save(book);
    }

    @Test
    public void update_whenExistingBookId_thenSaveShouldBeCalled() {
        Book book = Book.builder().id(1L).build();

        when(bookRepository.existsById(eq(1L))).thenReturn(true);
        bookService.update(1L, book);

        verify(bookRepository).save(book);
    }

    @Test
    public void update_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        Book book = Book.builder().id(1L).build();

        when(bookRepository.existsById(eq(1L))).thenReturn(false);

        assertThatThrownBy(() -> bookService.update(1L, book)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void delete_whenBookPassed_thenDeleteShouldBeCalled() {
        Book book = Book.builder().build();

        bookService.delete(book);

        verify(bookRepository).delete(book);
    }
}
