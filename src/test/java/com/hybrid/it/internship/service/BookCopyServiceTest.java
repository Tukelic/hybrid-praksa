package com.hybrid.it.internship.service;

import com.hybrid.it.internship.converter.BookCopyConverterServiceImpl;
import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.repository.BookCopyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookCopyServiceTest {

    @InjectMocks
    private BookCopyService bookCopyService;

    @Mock
    private BookCopyRepository bookCopyRepository;

    @Mock
    private BookCopyConverterServiceImpl bookCopyConverterService;

    @Mock
    private BookService bookService;

    @Test
    public void findById_whenExistingId_thenBookCopyShouldBeFound() {
        BookCopy bookCopy = BookCopy.builder().id(1L).build();

        when(bookCopyRepository.findById(eq(1L))).thenReturn(Optional.of(bookCopy));
        BookCopy found = bookCopyService.findById(1L);

        assertThat(found).isEqualTo(bookCopy);
    }

    @Test
    public void findById_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        when(bookCopyRepository.findById(eq(1L))).thenThrow(new NotFoundException());

        assertThatThrownBy(() -> bookCopyService.findById(1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void create_whenNewBookCopyDTO_thenSaveShouldBeCalled() {
        Book book = Book.builder().id(1L).build();
        BookCopy bookCopy = BookCopy.builder().hybridId("test-123").book(book).build();

        bookCopyService.create(bookCopy);

        verify(bookCopyRepository).save(bookCopy);
    }

    @Test
    public void update_whenExistingBookCopyId_thenSaveShouldBeCalled() {
        BookCopy bookCopy = BookCopy.builder().id(1L).build();

        when(bookCopyRepository.existsById(eq(1L))).thenReturn(true);
        bookCopyService.update(1L, bookCopy);

        verify(bookCopyRepository).save(bookCopy);
    }

    @Test
    public void update_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        BookCopy bookCopy = BookCopy.builder().id(1L).build();

        when(bookCopyRepository.existsById(eq(1L))).thenReturn(false);

        assertThatThrownBy(() -> bookCopyService.update(1L, bookCopy)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void delete_whenBookCopyPassed_thenDeleteShouldBeCalled() {
        BookCopy bookCopy = BookCopy.builder().build();

        bookCopyService.delete(bookCopy);

        verify(bookCopyRepository).delete(bookCopy);
    }


}
