package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.LeaseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LeaseServiceTest {

    @InjectMocks
    private LeaseService leaseService;

    @Mock
    private BookService bookService;

    @Mock
    private BookCopyService bookCopyService;

    @Mock
    private LeaseRepository leaseRepository;

    @Mock
    private LeaseValidationService leaseValidationService;

    @Test
    public void findById_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        when(leaseRepository.findById(eq(1L))).thenReturn(Optional.empty());

        assertThatThrownBy(() -> leaseService.findById(1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void leaseBooks_whenNoBookCopiesAvailable_thenNotFoundExceptionReturned() {
        BookCopy bookCopy1 = BookCopy.builder().id(1L).lease(new Lease()).build();
        Book book1 = Book.builder().id(1L).bookCopies(Set.of(bookCopy1)).build();
        bookCopy1.setBook(book1);
        User user = User.builder().id(1L).build();
        Lease lease = new Lease(user);

        when(leaseValidationService.getValidUser(1, 1L)).thenReturn(user);
        when(bookService.findById(1L)).thenReturn(book1);

        assertThatThrownBy(() -> leaseService.leaseBooks(List.of(1L), 1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void leaseBooks_whenMultipleBookIdsAndUserIdPassed_thenLeaseCreatedAndBookCopiesLeased() {
        BookCopy bookCopy1 = BookCopy.builder().id(1L).build();
        BookCopy bookCopy2 = BookCopy.builder().id(2L).build();
        Book book1 = Book.builder().id(1L).bookCopies(Set.of(bookCopy1)).build();
        Book book2 = Book.builder().id(2L).bookCopies(Set.of(bookCopy2)).build();
        bookCopy1.setBook(book1);
        bookCopy2.setBook(book2);
        User user = User.builder().id(1L).build();
        Lease lease = new Lease(user, Set.of(bookCopy1, bookCopy2));
        lease.setId(1L);

        when(leaseValidationService.getValidUser(2, 1L)).thenReturn(user);
        when(bookService.findById(1L)).thenReturn(book1);
        when(bookService.findById(2L)).thenReturn(book2);
        leaseService.leaseBooks(List.of(1L, 2L), 1L);

        verify(bookCopyService).update(1L, bookCopy1);
        verify(bookCopyService).update(2L, bookCopy2);
        verify(leaseRepository).save(any(Lease.class));
    }

    @Test
    public void returnLeasedBooks_whenBookCopyIdExists_thenBookCopyUpdated() {
        Lease lease = new Lease();
        BookCopy bookCopy1 = BookCopy.builder().id(1L).lease(lease).build();
        BookCopy bookCopy2 = BookCopy.builder().id(2L).lease(lease).build();
        lease.addBookCopy(bookCopy1);
        lease.addBookCopy(bookCopy2);

        when(bookCopyService.findByIdWhereLeaseExists(eq(1L))).thenReturn(bookCopy1);
        leaseService.returnLeasedBook(1L);

        verify(bookCopyService).update(1L, bookCopy1);
        verify(leaseRepository, never()).save(any());
        assertThat(bookCopy1.getLease()).isNull();
    }

    @Test
    public void returnLeasedBooks_whenAllBookCopiesInLeaseReturned_thenLeaseReturnDateTimeSet() {
        Lease lease = new Lease();
        BookCopy bookCopy1 = BookCopy.builder().id(1L).lease(lease).build();
        lease.addBookCopy(bookCopy1);

        when(bookCopyService.findByIdWhereLeaseExists(eq(1L))).thenReturn(bookCopy1);
        leaseService.returnLeasedBook(1L);

        verify(bookCopyService).update(1L, bookCopy1);
        verify(leaseRepository).save(lease);
        assertThat(lease.getReturnDateTime()).isNotNull();
    }
}
