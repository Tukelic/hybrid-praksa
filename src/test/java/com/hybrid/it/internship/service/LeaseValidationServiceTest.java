package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.BookLeasingException;
import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.LeaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LeaseValidationServiceTest {

    @InjectMocks
    private LeaseValidationService leaseValidationService;

    @Mock
    private LeaseRepository leaseRepository;

    @Mock
    private UserService userService;

    @Before
    public void beforeAll() {
        ReflectionTestUtils.setField(leaseValidationService, "leaseTimeAmount", 30);
        ReflectionTestUtils.setField(leaseValidationService, "leaseTimeUnit", ChronoUnit.DAYS);
    }

    @Test
    public void validateUserLease_whenNonExistentUserId_thenNotFoundExceptionShouldBeThrown() {
        when(userService.findById(eq(1L))).thenThrow(new NotFoundException());

        assertThatThrownBy(() -> leaseValidationService.getValidUser(1, 1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void validateUserLease_whenMoreThanThreeBooksToBeLeased_thenBookLeasingExceptionReturned() {
        BookCopy bookCopy1 = BookCopy.builder().id(1L).build();
        BookCopy bookCopy2 = BookCopy.builder().id(2L).build();
        BookCopy bookCopy3 = BookCopy.builder().id(3L).build();
        User user = User.builder().id(1L).build();
        Lease lease1 = new Lease(user, Set.of(bookCopy1, bookCopy2, bookCopy3));

        when(userService.findById(1L)).thenReturn(user);
        when(leaseRepository.findAllByUserAndReturnDateTimeIsNull(user)).thenReturn(List.of(lease1));

        assertThatThrownBy(() -> leaseValidationService.getValidUser(1, 1L))
                .isInstanceOf(BookLeasingException.class).hasMessage("User can lease 3 books max. Currently leased: 3 books");
    }

    @Test
    public void validateUserLease_whenOverdueLeaseExists_thenBookLeasingExceptionReturned() {
        BookCopy bookCopy1 = BookCopy.builder().id(1L).build();
        User user = User.builder().id(1L).build();
        Lease lease1 = new Lease(user, Set.of(bookCopy1));
        lease1.setLeaseDateTime(LocalDateTime.now().minusDays(31));

        when(userService.findById(1L)).thenReturn(user);
        when(leaseRepository.findAllByUserAndReturnDateTimeIsNull(user)).thenReturn(List.of(lease1));

        assertThatThrownBy(() -> leaseValidationService.getValidUser(1, 1L))
                .isInstanceOf(BookLeasingException.class).hasMessage("User has overdue books");
    }

    @Test
    public void validateUserLease_whenUserValidationSuccessful_thenUserReturned() {
        BookCopy bookCopy1 = BookCopy.builder().id(1L).build();
        User user = User.builder().id(1L).build();
        Lease lease1 = new Lease(user, Set.of(bookCopy1));

        when(userService.findById(1L)).thenReturn(user);
        when(leaseRepository.findAllByUserAndReturnDateTimeIsNull(user)).thenReturn(List.of(lease1));
        User user2 = leaseValidationService.getValidUser(1, 1L);

        assertThat(user2.getId()).isEqualTo(1L);
    }
}
