package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.ConstraintViolationException;
import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void findById_whenExistingId_thenUserShouldBeFound() {
        User user = User.builder().id(1L).build();

        when(userRepository.findById(eq(1L))).thenReturn(Optional.of(user));
        User found = userService.findById(1L);

        assertThat(found).isEqualTo(user);
    }

    @Test
    public void findById_whenNonExistentId_thenNotFoundExceptionShouldBeThrown() {
        when(userRepository.findById(eq(1L))).thenThrow(new NotFoundException());

        assertThatThrownBy(() -> userService.findById(1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void create_whenNewUser_thenSaveShouldBeCalled() {
        User user = User.builder().build();

        userService.create(user);

        verify(userRepository).findByUsernameOrEmail(user.getUsername(), user.getEmail());
        verify(userRepository).save(user);
    }

    @Test
    public void create_whenUsernameOrEmailTakenByDifferentUser_thenConstraintViolationExceptionShouldBeThrown() {
        User user = User.builder().id(1L).username("existing_usr").email("email@email.com").build();

        when(userRepository.findByUsernameOrEmail(eq("existing_usr"), eq("email@email.com")))
                .thenReturn(Optional.of(User.builder().id(2L).build()));

        assertThatThrownBy(() -> userService.create(user)).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void update_whenExistingUserId_thenSaveShouldBeCalled() {
        User user = User.builder().id(1L).build();

        when(userRepository.existsById(eq(1L))).thenReturn(true);
        userService.update(1L, user);

        verify(userRepository).save(user);
    }

    @Test
    public void update_whenNonExistingId_thenNotFoundExceptionShouldBeThrown() {
        User user = User.builder().id(1L).build();

        when(userRepository.existsById(eq(1L))).thenReturn(false);

        assertThatThrownBy(() -> userService.update(1L, user)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void update_whenUsernameOrEmailTakenByDifferentUser_thenConstraintViolationExceptionShouldBeThrown() {
        User user = User.builder().id(1L).username("existing_usr").email("email@email.com").build();

        when(userRepository.existsById(eq(1L))).thenReturn(true);
        when(userRepository.findByUsernameOrEmail(eq("existing_usr"), eq("email@email.com")))
                .thenReturn(Optional.of(User.builder().id(2L).build()));

        assertThatThrownBy(() -> userService.update(1L, user)).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void delete_whenUserPassed_thenDeleteShouldBeCalled() {
        User user = User.builder().build();

        userService.delete(user);

        verify(userRepository).delete(user);
    }
}
