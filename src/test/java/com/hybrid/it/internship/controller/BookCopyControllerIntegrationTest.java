package com.hybrid.it.internship.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.dto.BookDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookCopyControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findAll_whenCalledWithoutBookId_then200AndEmptyBodyIsReturned() throws Exception {
        mvc.perform(get("/books/copies")
                .queryParam("page", "0")
                .queryParam("size", "5"))
                .andExpect(status().isOk());
    }

    @Test
    public void findAll_whenCalledWithNonExistingBookId_then404IsReturned() throws Exception {
        mvc.perform(get("/books/copies")
                .queryParam("page", "0")
                .queryParam("size", "5")
                .queryParam("bookId", "1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findAll_whenExistingBookId_thenAllBookCopiesAreReceived() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/books/copies/1")
                .queryParam("page", "0")
                .queryParam("size", "5")
                .queryParam("bookId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$").isNotEmpty());
    }


    @Test
    public void findById_whenNonExistingId_then404isReturned() throws Exception {
        mvc.perform(get("/books/copies/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findById_whenExistingId_thenBookCopyIsReceived() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/books/copies/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hybridId").value("test-123"))
                .andExpect(jsonPath("$.bookId").value(1));
    }

    @Test
    public void create_whenBookCopyWithNonExistingBookId_then404IsReturned() throws Exception {
        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void create_whenBookCopy_thenBookCopyCreated() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void update_whenNonExistingId_then404IsReturned() throws Exception {
        mvc.perform(put("/books/copies/1")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .id(1L)
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_whenExistingId_thenUpdatedBookCopyIsReturned() throws Exception {
        BookCopyDTO bookCopyDTO = BookCopyDTO.builder().id(1L).hybridId("test-123").bookId(1L).build();

        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(bookCopyDTO))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(put("/books/copies/1")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .id(1L)
                                .hybridId("updated-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.hybridId").value("updated-123"))
                .andExpect(jsonPath("$.bookId").value(1));
    }

    @Test
    public void delete_whenNonExistingId_then404IsReturned() throws Exception {
        mvc.perform(delete("/books/copies/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenExistingId_then204IsReturned() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(delete("/books/copies/1"))
                .andExpect(status().isNoContent());
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return this.objectMapper.writeValueAsString(obj);
    }
}
