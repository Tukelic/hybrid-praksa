package com.hybrid.it.internship.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybrid.it.internship.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class LeaseControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void leaseBooks_whenNonExistingUserId_then404IsReturned() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/leases")
                .content(asJsonString(new BooksToLeaseDTO(List.of(1L), 1L)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void leaseBooks_whenNonExistingBookId_then404IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/leases")
                .content(asJsonString(new BooksToLeaseDTO(List.of(1L), 1L)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void leaseBooks_whenExistingBookCopyid_then200IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(
                        BookCopyDTO.builder()
                                .id(1L)
                                .hybridId("test-123")
                                .bookId(1L)
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));


        mvc.perform(post("/leases")
                .content(asJsonString(new BooksToLeaseDTO(List.of(1L), 1L)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.leaseDateTime").exists())
                .andExpect(jsonPath("$.bookCopyDTOs").isNotEmpty());
    }

    @Test
    public void returnBookCopy_whenLeasedBookCopyId_then200IsReturned() throws Exception {
        BookCopyDTO bookCopyDTO1 = BookCopyDTO.builder().id(1L).hybridId("test-123").bookId(1L).build();

        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books/copies")
                .content(asJsonString(bookCopyDTO1))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/leases")
                .content(asJsonString(new BooksToLeaseDTO(List.of(1L), 1L)))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/leases/return")
                .queryParam("userId", "1")
                .content(asJsonString(new ReturnBookCopyDTO(1L)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.leaseDateTime").exists())
                .andExpect(jsonPath("$.bookCopyDTOs").isEmpty());
    }

    @Test
    public void returnBookCopy_whenBookCopyIdNonExistent_then404IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/leases/return")
                .queryParam("userId", "1")
                .content(asJsonString(new ReturnBookCopyDTO(1L)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return this.objectMapper.writeValueAsString(obj);
    }
}
