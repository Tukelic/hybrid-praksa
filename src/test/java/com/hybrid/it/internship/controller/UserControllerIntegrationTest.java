package com.hybrid.it.internship.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybrid.it.internship.dto.UserDTO;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findAll_whenCalled_then200AndEmptyBodyIsReturned() throws Exception {
        mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void findById_whenNonExistingId_then404isReturned() throws Exception {
        mvc.perform(get("/users/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void create_whenUserPassed_thenUserCreated() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void create_whenExistingUsername_then403IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("SameUsername")
                                .password("AnyPassword")
                                .firstName("AnyFirstName")
                                .lastName("AnyLastName")
                                .email("any@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("SameUsername")
                                .password("DifferentPassword")
                                .firstName("DifferentFirstName")
                                .lastName("DifferentLastName")
                                .email("different@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void create_whenExistingEmail_then403IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("AnyUsername")
                                .password("AnyPassword")
                                .firstName("AnyFirstName")
                                .lastName("AnyLastName")
                                .email("same@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("DifferentUsername")
                                .password("DifferentPassword")
                                .firstName("DifferentFirstName")
                                .lastName("DifferentLastName")
                                .email("same@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void findById_whenExistingId_thenUserIsReceived() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.username").value("TestUsername"))
                .andExpect(jsonPath("$.password").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.firstName").value("TestFirstName"))
                .andExpect(jsonPath("$.lastName").value("TestLastName"))
                .andExpect(jsonPath("$.email").value("test@email.com"));
    }

    @Test
    public void update_whenNonExistingId_then404isReturned() throws Exception {
        mvc.perform(put("/users/1")
                .content(asJsonString(
                        UserDTO.builder()
                                .id(1L)
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_whenExistingId_thenUpdatedUserIsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(put("/users/1")
                .content(asJsonString(
                        UserDTO.builder()
                                .id(1L)
                                .username("UpdatedUsername")
                                .password("UpdatedPassword")
                                .firstName("UpdatedFirstName")
                                .lastName("UpdatedLastName")
                                .email("updated@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.username").value("UpdatedUsername"))
                .andExpect(jsonPath("$.password").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.firstName").value("UpdatedFirstName"))
                .andExpect(jsonPath("$.lastName").value("UpdatedLastName"))
                .andExpect(jsonPath("$.email").value("updated@email.com"));
    }

    @Test
    public void update_whenExistingUsernameAndDifferentUser_then403IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("SameUsername")
                                .password("AnyPassword")
                                .firstName("AnyFirstName")
                                .lastName("AnyLastName")
                                .email("any@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("SecondUsername")
                                .password("SecondPassword")
                                .firstName("SecondFirstName")
                                .lastName("SecondLastName")
                                .email("second@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(put("/users/2")
                .content(asJsonString(
                        UserDTO.builder()
                                .id(2L)
                                .username("SameUsername")
                                .password("DifferentPassword")
                                .firstName("DifferentFirstName")
                                .lastName("DifferentLastName")
                                .email("different@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void update_whenExistingEmail_then403IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("AnyUsername")
                                .password("AnyPassword")
                                .firstName("AnyFirstName")
                                .lastName("AnyLastName")
                                .email("same@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("SecondUsername")
                                .password("SecondPassword")
                                .firstName("SecondFirstName")
                                .lastName("SecondLastName")
                                .email("second@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(put("/users/2")
                .content(asJsonString(
                        UserDTO.builder()
                                .id(2L)
                                .username("DifferentUsername")
                                .password("DifferentPassword")
                                .firstName("DifferentFirstName")
                                .lastName("DifferentLastName")
                                .email("same@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void delete_whenNonExistingId_then404IsReturned() throws Exception {
        mvc.perform(delete("/users/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenExistingId_then204IsReturned() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(
                        UserDTO.builder()
                                .username("TestUsername")
                                .password("TestPassword")
                                .firstName("TestFirstName")
                                .lastName("TestLastName")
                                .email("test@email.com")
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(delete("/users/1"))
                .andExpect(status().isNoContent());
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return this.objectMapper.writeValueAsString(obj);
    }
}
