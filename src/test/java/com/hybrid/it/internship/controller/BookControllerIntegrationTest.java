package com.hybrid.it.internship.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybrid.it.internship.dto.BookDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findAll_whenCalledWithoutIsbn_then200AndEmptyBodyIsReturned() throws Exception {
        mvc.perform(get("/books"))
                .andExpect(status().isOk());
    }

    @Test
    public void findAll_whenCalledWithExistingIsbn_then200AndListWithBooksIsReturned() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/books")
                .queryParam("isbn", "123-4-56"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void findById_whenNonExistingId_then404isReturned() throws Exception {
        mvc.perform(get("/books/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void create_whenBookPassed_thenBookCreated() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void findById_whenExistingId_thenBookIsReceived() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(get("/books/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("Test title"))
                .andExpect(jsonPath("$.author").value("Test author"))
                .andExpect(jsonPath("$.isbn").value("123-4-56-789012-3"));
    }

    @Test
    public void update_whenNonExistingId_then404isReturned() throws Exception {
        mvc.perform(put("/books/1")
                .content(asJsonString(
                        BookDTO.builder()
                                .id(1L)
                                .title("Updated title")
                                .author("Updated author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_whenExistingId_thenUpdatedBookIsReturned() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(put("/books/1")
                .content(asJsonString(
                        BookDTO.builder()
                                .id(1L)
                                .title("Updated title")
                                .author("Updated author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("Updated title"))
                .andExpect(jsonPath("$.author").value("Updated author"));
    }

    @Test
    public void delete_whenNonExistingId_then404IsReturned() throws Exception {
        mvc.perform(delete("/books/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_whenExistingId_then204IsReturned() throws Exception {
        mvc.perform(post("/books")
                .content(asJsonString(
                        BookDTO.builder()
                                .title("Test title")
                                .author("Test author")
                                .isbn("123-4-56-789012-3")
                                .bookCopies(new HashSet<>())
                                .build()))
                .contentType(MediaType.APPLICATION_JSON));

        mvc.perform(delete("/books/1"))
                .andExpect(status().isNoContent());
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return this.objectMapper.writeValueAsString(obj);
    }

}
