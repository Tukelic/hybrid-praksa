package com.hybrid.it.internship.suites;

import com.hybrid.it.internship.service.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BookCopyServiceTest.class,
        BookServiceTest.class,
        LeaseServiceTest.class,
        LeaseValidationServiceTest.class,
        UserServiceTest.class
})
public class ServiceTestSuite {
}
