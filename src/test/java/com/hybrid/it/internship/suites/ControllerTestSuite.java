package com.hybrid.it.internship.suites;

import com.hybrid.it.internship.controller.BookControllerIntegrationTest;
import com.hybrid.it.internship.controller.BookCopyControllerIntegrationTest;
import com.hybrid.it.internship.controller.LeaseControllerIntegrationTest;
import com.hybrid.it.internship.controller.UserControllerIntegrationTest;
import com.hybrid.it.internship.service.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        BookControllerIntegrationTest.class,
        BookCopyControllerIntegrationTest.class,
        LeaseControllerIntegrationTest.class,
        UserControllerIntegrationTest.class
})
public class ControllerTestSuite {
}
