package com.hybrid.it.internship.suites;

import com.hybrid.it.internship.converter.BookConverterServiceImplTest;
import com.hybrid.it.internship.converter.BookCopyConverterServiceImplTest;
import com.hybrid.it.internship.converter.UserConverterServiceImplTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BookConverterServiceImplTest.class,
        BookCopyConverterServiceImplTest.class,
        UserConverterServiceImplTest.class,
})
public class ConverterServiceTestSuite {
}
