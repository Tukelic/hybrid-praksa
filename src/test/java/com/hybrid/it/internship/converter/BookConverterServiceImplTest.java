package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BookDTO;
import com.hybrid.it.internship.model.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BookConverterServiceImplTest {

    @InjectMocks
    private BookConverterServiceImpl bookConverterService;

    @Mock
    private BookCopyConverterServiceImpl bookCopyConverterService;

    @Test
    public void convertToDto_whenBook_thenBookDTOShouldBeCreated() {
        Book book = Book.builder()
                .id(1L)
                .title("Test title")
                .author("Test author")
                .isbn("123-4-56-789012-3")
                .bookCopies(new HashSet<>())
                .build();

        BookDTO bookDTO = bookConverterService.convertToDto(book);

        assertThat(bookDTO.getId()).isEqualTo(Long.valueOf(1));
        assertThat(bookDTO.getCreationDateTime()).isEqualTo(book.getCreationDateTime());
        assertThat(bookDTO.getTitle()).isEqualTo("Test title");
        assertThat(bookDTO.getAuthor()).isEqualTo("Test author");
        assertThat(bookDTO.getIsbn()).isEqualTo("123-4-56-789012-3");
    }

    @Test
    public void convertToEntity_whenBookDTO_thenBookShouldBeCreated() {
        BookDTO bookDTO = BookDTO.builder()
                .id(1L)
                .creationDateTime(LocalDateTime.now().minusHours(1))
                .title("Test title")
                .author("Test author")
                .isbn("123-4-56-789012-3")
                .bookCopies(new HashSet<>())
                .build();

        Book book = bookConverterService.convertToEntity(bookDTO);

        assertThat(book.getId()).isEqualTo(Long.valueOf(1));
        assertThat(book.getTitle()).isEqualTo("Test title");
        assertThat(book.getAuthor()).isEqualTo("Test author");
        assertThat(book.getIsbn()).isEqualTo("123-4-56-789012-3");
        assertThat(book.getCreationDateTime()).isNotEqualTo(bookDTO.getCreationDateTime());
    }
}
