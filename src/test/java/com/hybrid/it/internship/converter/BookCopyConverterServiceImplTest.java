package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookCopyConverterServiceImplTest {

    @InjectMocks
    private BookCopyConverterServiceImpl bookCopyConverterService;

    @Mock
    private BookService bookService;

    @Test
    public void convertToDto_whenBookCopy_thenBookCopyDTOShouldBeReturned() {
        BookCopy bookCopy = BookCopy.builder()
                .id(1L)
                .hybridId("ns-123")
                .book(Book.builder().id(1L).build())
                .build();

        BookCopyDTO bookCopyDTO = bookCopyConverterService.convertToDto(bookCopy);

        assertThat(bookCopyDTO.getId()).isEqualTo(Long.valueOf(1));
        assertThat(bookCopyDTO.getHybridId()).isEqualTo("ns-123");
        assertThat(bookCopyDTO.getBookId()).isEqualTo(Long.valueOf(1));
    }

    @Test
    public void convertToEntity_whenBookCopyDTO_thenBookCopyShouldBeCreated() {
        Book book = Book.builder().id(1L).build();
        BookCopyDTO bookCopyDTO = BookCopyDTO.builder()
                .id(1L)
                .hybridId("ns-123")
                .bookId(1L)
                .build();

        when(bookService.findById(1L)).thenReturn(book);
        BookCopy bookCopy = bookCopyConverterService.convertToEntity(bookCopyDTO);

        assertThat(bookCopy.getId()).isEqualTo(Long.valueOf(1));
        assertThat(bookCopy.getHybridId()).isEqualTo("ns-123");
        assertThat(bookCopy.getBook()).isEqualTo(book);
    }
}
