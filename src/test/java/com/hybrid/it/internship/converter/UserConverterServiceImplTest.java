package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.UserDTO;
import com.hybrid.it.internship.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterServiceImplTest {

    @InjectMocks
    private UserConverterServiceImpl userConverterService;

    @Test
    public void convertToDto_whenUser_thenUserDTOShouldBeCreated() {
        User user = User.builder()
                .id(1L)
                .username("TestUsername")
                .password("TestPassword")
                .firstName("TestFirstName")
                .lastName("TestLastName")
                .email("test@email.com")
                .build();

        UserDTO userDTO = userConverterService.convertToDto(user);

        assertThat(userDTO.getId()).isEqualTo(Long.valueOf(1));
        assertThat(userDTO.getUsername()).isEqualTo("TestUsername");
        assertThat(userDTO.getPassword()).isNull();
        assertThat(userDTO.getFirstName()).isEqualTo("TestFirstName");
        assertThat(userDTO.getLastName()).isEqualTo("TestLastName");
        assertThat(userDTO.getEmail()).isEqualTo("test@email.com");
    }

    @Test
    public void convertToEntity_whenUserDto_thenUserShouldBeCreated() {
        UserDTO userDTO = UserDTO.builder()
                .id(1L)
                .username("TestUsername")
                .password("TestPassword")
                .firstName("TestFirstName")
                .lastName("TestLastName")
                .email("test@email.com")
                .build();

        User user = userConverterService.convertToEntity(userDTO);

        assertThat(user.getId()).isEqualTo(Long.valueOf(1));
        assertThat(user.getUsername()).isEqualTo("TestUsername");
        assertThat(user.getPassword()).isEqualTo("TestPassword");
        assertThat(user.getFirstName()).isEqualTo("TestFirstName");
        assertThat(user.getLastName()).isEqualTo("TestLastName");
        assertThat(user.getEmail()).isEqualTo("test@email.com");
    }
}
