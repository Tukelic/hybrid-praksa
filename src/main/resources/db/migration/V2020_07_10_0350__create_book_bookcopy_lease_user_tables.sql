CREATE TABLE book (
  id                    SERIAL          PRIMARY KEY,
  creation_date_time    TIMESTAMP       NOT NULL,
  title                 VARCHAR         NOT NULL,
  author                VARCHAR         NOT NULL,
  isbn                  VARCHAR(17)     UNIQUE NOT NULL
);

CREATE TABLE "user" (
  id                SERIAL              PRIMARY KEY,
  username          VARCHAR(100)        UNIQUE NOT NULL,
  password          VARCHAR(100)        NOT NULL,
  first_name        VARCHAR(100)        NOT NULL,
  last_name         VARCHAR(100)        NOT NULL,
  email             VARCHAR(100)        UNIQUE NOT NULL
);

CREATE TABLE lease (
  id                    SERIAL          PRIMARY KEY,
  user_id               INTEGER         REFERENCES "user"(id) NOT NULL,
  lease_date_time       TIMESTAMP       NOT NULL,
  return_date_time      TIMESTAMP
);

CREATE TABLE book_copy (
  id                SERIAL          PRIMARY KEY,
  hybrid_id         VARCHAR(100)    NOT NULL,
  book_id           INTEGER         REFERENCES book(id) NOT NULL,
  lease_id          INTEGER         REFERENCES lease(id)
);

