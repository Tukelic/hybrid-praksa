package com.hybrid.it.internship.controller;

import com.hybrid.it.internship.converter.ConverterService;
import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.service.BookCopyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

@RestController
@RequestMapping(value = "/books/copies")
public class BookCopyController {

    private final BookCopyService bookCopyService;
    private final ConverterService<BookCopyDTO, BookCopy> bookCopyConverter;

    public BookCopyController(BookCopyService bookCopyService, ConverterService<BookCopyDTO, BookCopy> bookCopyConverter) {
        this.bookCopyService = bookCopyService;
        this.bookCopyConverter = bookCopyConverter;
    }

    @Operation(summary = "Get book copies by page. If param is passed, then all books for given book id are returned")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<BookCopyDTO> findAll(@RequestParam Optional<Long> bookId, Pageable pageable) {
        return bookId.map(id -> bookCopyService.findAllByBookId(id, pageable))
                .orElseGet(() -> bookCopyService.findAll(pageable))
                .map(bookCopyConverter::convertToDto);
    }

    @Operation(summary = "Get a book copy by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book copy",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookCopyDTO findById(@PathVariable("id") Long id) {
        return bookCopyConverter.convertToDto(bookCopyService.findById(id));
    }

    @Operation(summary = "Create new book copy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Book copy created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book for given id not found",
                    content = @Content)})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookCopyDTO create(@Valid @RequestBody BookCopyDTO bookCopyDTO) {
        return bookCopyConverter.convertToDto(bookCopyService.create(bookCopyConverter.convertToEntity(bookCopyDTO)));
    }

    @Operation(summary = "Update existing book copy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book copy updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookCopyDTO update(@PathVariable("id") @NotNull @Positive Long id, @Valid @RequestBody BookCopyDTO bookCopyDTO) {
        return bookCopyConverter.convertToDto(bookCopyService.update(id, bookCopyConverter.convertToEntity(bookCopyDTO)));
    }

    @Operation(summary = "Delete book copy by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Book copy deleted",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        bookCopyService.delete(bookCopyService.findById(id));
    }
}
