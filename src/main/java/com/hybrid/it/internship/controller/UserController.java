package com.hybrid.it.internship.controller;

import com.hybrid.it.internship.converter.ConverterService;
import com.hybrid.it.internship.dto.UserDTO;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;
    private final ConverterService<UserDTO, User> userConverter;

    public UserController(UserService userService, ConverterService<UserDTO, User> userConverter) {
        this.userService = userService;
        this.userConverter = userConverter;
    }

    @Operation(summary = "Get users by page")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> findAll(Pageable pageable) {
        return userService.findAll(pageable)
                .stream()
                .map(userConverter::convertToDto)
                .collect(Collectors.toList());
    }

    @Operation(summary = "Get user by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO findById(@PathVariable("id") @NotNull @Positive Long id) {
        return userConverter.convertToDto(userService.findById(id));
    }

    @Operation(summary = "Create new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "409", description = "Different user with same username/email",
                    content = @Content)})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO create(@Valid @RequestBody UserDTO userDTO) {
        return userConverter.convertToDto(userService.create(userConverter.convertToEntity(userDTO)));
    }

    @Operation(summary = "Update existing user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "409", description = "Different user with same username/email",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO update(@PathVariable("id") @NotNull @Positive Long id, @Valid @RequestBody UserDTO userDTO) {
        return userConverter.convertToDto(userService.update(id, userConverter.convertToEntity(userDTO)));
    }

    @Operation(summary = "Delete user by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") @NotNull @Positive Long id) {
        userService.delete(userService.findById(id));
    }
}
