package com.hybrid.it.internship.controller;

import com.hybrid.it.internship.converter.ConverterService;
import com.hybrid.it.internship.dto.BookDTO;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

@RestController
@RequestMapping(value = "/books")
public class BookController {

    private final BookService bookService;
    private final ConverterService<BookDTO, Book> bookConverter;

    public BookController(BookService bookService, ConverterService<BookDTO, Book> bookConverter) {
        this.bookService = bookService;
        this.bookConverter = bookConverter;
    }

    @Operation(summary = "Get books by page. If isbn param is passed, then all books that contain that isbn are returned")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<BookDTO> findAll(@RequestParam Optional<String> isbn, Pageable pageable) {
        return isbn.map(isbnParam -> bookService.findAllByIsbn(isbnParam, pageable))
                .orElseGet(() -> bookService.findAll(pageable))
                .map(bookConverter::convertToDto);
    }

    @Operation(summary = "Get a book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookDTO findById(@PathVariable("id") Long id) {
        return bookConverter.convertToDto(bookService.findById(id));
    }

    @Operation(summary = "Create new book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Book created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))})})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookDTO create(@Valid @RequestBody BookDTO bookDTO) {
        return bookConverter.convertToDto(bookService.create(bookConverter.convertToEntity(bookDTO)));
    }

    @Operation(summary = "Update existing book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookDTO update(@PathVariable("id") @NotNull @Positive Long id, @Valid @RequestBody BookDTO bookDTO) {
        return bookConverter.convertToDto(bookService.update(id, bookConverter.convertToEntity(bookDTO)));
    }

    @Operation(summary = "Delete book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Book deleted",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        bookService.delete(bookService.findById(id));
    }
}
