package com.hybrid.it.internship.controller;

import com.hybrid.it.internship.converter.ConverterService;
import com.hybrid.it.internship.dto.BooksToLeaseDTO;
import com.hybrid.it.internship.dto.LeaseDTO;
import com.hybrid.it.internship.dto.ReturnBookCopyDTO;
import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.service.LeaseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/leases")
public class LeaseController {

    private final LeaseService leaseService;

    private final ConverterService<LeaseDTO, Lease> leaseConverterService;

    public LeaseController(LeaseService leaseService, ConverterService<LeaseDTO, Lease> leaseConverterService) {
        this.leaseService = leaseService;
        this.leaseConverterService = leaseConverterService;
    }

    @Operation(summary = "Lease books")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Books leased",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Book not found/No books available",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "User not allowed to lease",
                    content = @Content)})
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public LeaseDTO leaseBooks(@RequestBody @Valid BooksToLeaseDTO booksToLeaseDTO) {
        return leaseConverterService.convertToDto(leaseService.leaseBooks(booksToLeaseDTO.getBookIds(), booksToLeaseDTO.getUserId()));
    }

    @Operation(summary = "Return leased book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Lease returned",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content)})
    @PostMapping(value = "/return")
    @ResponseStatus(HttpStatus.OK)
    public LeaseDTO returnBookCopy(@RequestBody @Valid ReturnBookCopyDTO returnBookCopyDTO) {
        return leaseConverterService.convertToDto(leaseService.returnLeasedBook(returnBookCopyDTO.getBookCopyId()));
    }

}
