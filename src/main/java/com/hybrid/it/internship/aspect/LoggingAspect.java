package com.hybrid.it.internship.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Aspect
@Component
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("@annotation(com.hybrid.it.internship.aspect.Trace)")
    public Object trace(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("Invoked method {} with arguments: {}", joinPoint.getSignature(), joinPoint.getArgs());
        logger.debug("Method {} started execution at: {}", joinPoint.getSignature(), LocalTime.now());

        Object proceed = joinPoint.proceed();

        logger.debug("Method {} finished execution at: {}", joinPoint.getSignature(), LocalTime.now());

        return proceed;
    }
}
