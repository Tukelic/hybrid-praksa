package com.hybrid.it.internship.repository;

import com.hybrid.it.internship.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    Page<Book> findAllByIsbnContaining(String isbn, Pageable pageable);

    List<Book> findAllByIdIn(List<Long> ids);
}
