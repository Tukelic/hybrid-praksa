package com.hybrid.it.internship.repository;

import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LeaseRepository extends JpaRepository<Lease, Long> {

    List<Lease> findAllByUserAndReturnDateTimeIsNull(User user);

}
