package com.hybrid.it.internship.repository;

import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.model.BookCopy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {

    Page<BookCopy> findAllByBook(Book book, Pageable pageable);

    Optional<BookCopy> findByIdAndLeaseIsNotNull(Long id);
}
