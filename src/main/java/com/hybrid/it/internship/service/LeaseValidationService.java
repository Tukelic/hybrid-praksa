package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.BookLeasingException;
import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.LeaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class LeaseValidationService {

    private long leaseTimeAmount;

    private ChronoUnit leaseTimeUnit;

    private LeaseRepository leaseRepository;

    private UserService userService;

    public LeaseValidationService() {
    }

    @Autowired
    public LeaseValidationService(LeaseRepository leaseRepository, UserService userService,
                                  @Value("${lease.time.amount}") long leaseTimeAmount, @Value("${lease.time.unit}") String leaseTimeUnit) {
        this.leaseRepository = leaseRepository;
        this.userService = userService;
        this.leaseTimeAmount = leaseTimeAmount;
        this.leaseTimeUnit = ChronoUnit.valueOf(leaseTimeUnit.toUpperCase());
    }

    public User getValidUser(int numberOfBooksToLease, Long userId) {
        User user = userService.findById(userId);
        validateUserLease(numberOfBooksToLease, user);
        return user;
    }

    public void validateUserLease(int numberOfBooksToLease, User user) {
        List<Lease> userLeases = leaseRepository.findAllByUserAndReturnDateTimeIsNull(user);
        long notReturnedBooks = userLeases.stream().mapToLong(lease -> lease.getBookCopies().size()).sum();

        checkLeaseLimit(notReturnedBooks, numberOfBooksToLease);
        checkOverdueBooks(userLeases);
    }

    private void checkLeaseLimit(long notReturnedBooks, int numberOfBookIds) {
        if (notReturnedBooks + numberOfBookIds > 3)
            throw new BookLeasingException(String.format("User can lease 3 books max. Currently leased: %d books", notReturnedBooks));
    }

    private void checkOverdueBooks(List<Lease> userLeases) {
        if (userLeases.stream().anyMatch(lease -> lease.isOverdue(leaseTimeAmount, leaseTimeUnit)))
            throw new BookLeasingException("User has overdue books");
    }
}
