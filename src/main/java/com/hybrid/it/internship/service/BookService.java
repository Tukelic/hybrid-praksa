package com.hybrid.it.internship.service;

import com.hybrid.it.internship.aspect.Trace;
import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

    private Logger logger = LoggerFactory.getLogger(BookService.class);

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public Page<Book> findAll(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    public Page<Book> findAllByIsbn(String isbn, Pageable pageable) {
        logger.debug("Looking for book with isbn: {}", isbn);
        return bookRepository.findAllByIsbnContaining(isbn, pageable);
    }

    @Trace
    public Book findById(Long id) {
        logger.debug("Looking for book with id: {}", id);
        return bookRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(String.format("Book with id %d not found", id));
        });
    }

    @Trace
    public Book create(Book book) {
        logger.debug("Creating new book {}", book);
        return bookRepository.save(book);
    }

    public Book update(Long id, Book book) {
        logger.debug("Updating book {}. Book id: {}", book, id);
        if (bookRepository.existsById(id)) {
            return bookRepository.save(book);
        } else {
            throw new NotFoundException(String.format("Book with id %d not found", id));
        }
    }

    @Transactional
    public void delete(Book book) {
        logger.debug("Deleting book with id: {}", book.getId());
        bookRepository.delete(book);
    }

}
