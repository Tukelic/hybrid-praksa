package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.repository.BookCopyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BookCopyService {

    private Logger logger = LoggerFactory.getLogger(BookCopyService.class);

    private final BookCopyRepository bookCopyRepository;

    private final BookService bookService;

    public BookCopyService(BookCopyRepository bookCopyRepository, BookService bookService) {
        this.bookCopyRepository = bookCopyRepository;
        this.bookService = bookService;
    }

    public Page<BookCopy> findAll(Pageable pageable) {
        return bookCopyRepository.findAll(pageable);
    }

    public Page<BookCopy> findAllByBookId(Long bookId, Pageable pageable) {
        return bookCopyRepository.findAllByBook(bookService.findById(bookId), pageable);
    }

    public BookCopy findById(Long id) {
        logger.debug("Looking for book copy with id: {}", id);
        return bookCopyRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(String.format("Book copy with id %d not found", id));
        });
    }

    public BookCopy findByIdWhereLeaseExists(Long id) {
        logger.debug("Looking for book copy with id: {}", id);
        return bookCopyRepository.findByIdAndLeaseIsNotNull(id).orElseThrow(() -> {
            throw new NotFoundException(String.format("Book copy with id %d not found", id));
        });
    }

    public BookCopy create(BookCopy bookCopy) {
        logger.debug("Creating new book copy {}", bookCopy);
        return bookCopyRepository.save(bookCopy);
    }

    public BookCopy update(Long id, BookCopy bookCopy) {
        logger.debug("Updating book copy {}. Book copy id: {}", bookCopy, id);
        if (bookCopyRepository.existsById(bookCopy.getId())) {
            return bookCopyRepository.save(bookCopy);
        } else {
            throw new NotFoundException(String.format("Book copy with id %d not found", id));
        }
    }

    @Transactional
    public void delete(BookCopy bookCopy) {
        logger.debug("Deleting book copy with id: {}", bookCopy.getId());
        bookCopyRepository.delete(bookCopy);
    }

}
