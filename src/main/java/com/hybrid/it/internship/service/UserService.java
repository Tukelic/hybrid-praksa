package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.ConstraintViolationException;
import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public User findById(Long id) {
        logger.debug("Looking for user with id: {}", id);
        return userRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(String.format("User with id %d not found", id));
        });
    }

    public User create(User user) {
        logger.debug("Creating new user {}", user);
        validateUser(user);
        return userRepository.save(user);
    }

    public User update(Long id, User user) {
        logger.debug("Updating user {}. User id: {}", user, id);
        if (userRepository.existsById(user.getId())) {
            validateUser(user);
            return userRepository.save(user);
        } else {
            throw new NotFoundException(String.format("User with id %d not found", id));
        }
    }

    @Transactional
    public void delete(User user) {
        logger.debug("Deleting user with id: {}", user.getId());
        userRepository.delete(user);
    }

    private void validateUser(User user) {
        userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail()).ifPresent(existing -> {
            if (existing.getId() != user.getId())
                throw new ConstraintViolationException(String.format("Username '%s' or email '%s' is taken", user.getUsername(), user.getEmail()));
        });
    }
}
