package com.hybrid.it.internship.service;

import com.hybrid.it.internship.exception.NotFoundException;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.model.Lease;
import com.hybrid.it.internship.model.User;
import com.hybrid.it.internship.repository.LeaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class LeaseService {

    private final Logger logger = LoggerFactory.getLogger(LeaseService.class);

    private final BookService bookService;

    private final BookCopyService bookCopyService;

    private final LeaseRepository leaseRepository;

    private final LeaseValidationService leaseValidationService;

    public LeaseService(BookService bookService, BookCopyService bookCopyService, LeaseRepository leaseRepository, LeaseValidationService leaseValidationService) {
        this.bookService = bookService;
        this.bookCopyService = bookCopyService;
        this.leaseRepository = leaseRepository;
        this.leaseValidationService = leaseValidationService;
    }

    public Lease findById(Long id) {
        return leaseRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Lease with id $d not found", id)));
    }

    @Transactional
    public Lease leaseBooks(List<Long> bookIds, Long userId) {
        User user = leaseValidationService.getValidUser(bookIds.size(), userId);
        Lease lease = new Lease(user);

        for (Long bookId : bookIds) {
            BookCopy foundBookCopy = bookService.findById(bookId).getBookCopies()
                    .stream()
                    .filter(BookCopy::isAvailableToLease)
                    .findAny()
                    .orElseThrow(() -> new NotFoundException(String.format("No book copies available for book with id %d", bookId)));

            foundBookCopy.setLease(lease);
            bookCopyService.update(foundBookCopy.getId(), foundBookCopy);

            lease.addBookCopy(foundBookCopy);
        }

        return leaseRepository.save(lease);
    }

    @Transactional
    public Lease returnLeasedBook(Long bookCopyId) {
        BookCopy bookCopy = bookCopyService.findByIdWhereLeaseExists(bookCopyId);
        Lease lease = bookCopy.getLease();

        bookCopy.setLease(null);
        bookCopyService.update(bookCopy.getId(), bookCopy);

        if (lease.getBookCopies().stream().noneMatch(BookCopy::isLeased)) {
            lease.setReturnDateTime(LocalDateTime.now());
            leaseRepository.save(lease);
        }
        return lease;
    }
}
