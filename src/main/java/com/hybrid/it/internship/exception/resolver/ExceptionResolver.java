package com.hybrid.it.internship.exception.resolver;

import com.hybrid.it.internship.dto.ErrorDTO;
import com.hybrid.it.internship.exception.BookLeasingException;
import com.hybrid.it.internship.exception.ConstraintViolationException;
import com.hybrid.it.internship.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionResolver {

    private static Logger logger = LoggerFactory.getLogger(ExceptionResolver.class);

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorDTO handleNotFoundException(NotFoundException e) {
        ErrorDTO errorDTO = ErrorDTO.builder()
                .message(e.getMessage())
                .internalStatusCode(3000)
                .timestamp(LocalDateTime.now())
                .build();
        logger.error("NotFound exception occured. ", e);
        return errorDTO;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorDTO handleMethodArgNotValidException(MethodArgumentNotValidException e) {
        ErrorDTO errorDTO = ErrorDTO.builder()
                .message("Field validation failed")
                .fieldErrors(e.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.toList()))
                .internalStatusCode(4000)
                .timestamp(LocalDateTime.now())
                .build();
        logger.error("MethodArgumentNotValid exception occured. ", e);
        return errorDTO;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public ErrorDTO handleConstraintViolationException(ConstraintViolationException e) {
        ErrorDTO errorDTO = ErrorDTO.builder()
                .message(e.getMessage())
                .internalStatusCode(4001)
                .timestamp(LocalDateTime.now())
                .build();
        logger.info("ConstraintViolationException exception occured. {}", e.getMessage());
        return errorDTO;
    }

    @ExceptionHandler(BookLeasingException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public ErrorDTO handleBookLeasingException(BookLeasingException e) {
        ErrorDTO errorDTO = ErrorDTO.builder()
                .message(e.getMessage())
                .internalStatusCode(4002)
                .timestamp(LocalDateTime.now())
                .build();
        logger.info("BookLeasingException exception occured. {}", e.getMessage());
        return errorDTO;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDTO handleAnyException(Exception e) {
        ErrorDTO errorDTO = ErrorDTO.builder()
                .message(e.getMessage())
                .internalStatusCode(5000)
                .timestamp(LocalDateTime.now())
                .build();
        logger.error("Unknown exception occured. ", e);
        return errorDTO;
    }
}
