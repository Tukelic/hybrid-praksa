package com.hybrid.it.internship.exception;

public class BookLeasingException extends RuntimeException {

    public BookLeasingException() {
        super();
    }

    public BookLeasingException(String message) {
        super(message);
    }

    public BookLeasingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookLeasingException(Throwable cause) {
        super(cause);
    }
}
