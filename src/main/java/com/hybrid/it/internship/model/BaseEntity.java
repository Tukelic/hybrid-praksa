package com.hybrid.it.internship.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public BaseEntity() {
    }

    public BaseEntity(Long id) {
        this.id = id;
    }

    public static abstract class BaseBuilder<ENTITY extends BaseEntity, BUILDER extends BaseBuilder> {

        protected Long id;

        @SuppressWarnings("unchecked")
        public BUILDER id(Long id) {
            this.id = id;
            return (BUILDER) this;
        }

        abstract ENTITY build();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
