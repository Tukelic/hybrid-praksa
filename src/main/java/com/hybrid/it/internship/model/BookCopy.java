package com.hybrid.it.internship.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import static java.util.Objects.isNull;

@Entity
public class BookCopy extends BaseEntity {

    private String hybridId;

    @ManyToOne
    private Book book;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Lease lease;

    protected BookCopy() {
        super();
    }

    protected BookCopy(BookCopyBuilder bookCopyBuilder) {
        super(bookCopyBuilder.id);
        this.hybridId = bookCopyBuilder.hybridId;
        this.book = bookCopyBuilder.book;
        this.lease = bookCopyBuilder.lease;
    }

    public static BookCopyBuilder builder() {
        return new BookCopyBuilder();
    }

    public boolean isAvailableToLease() {
        return this.lease == null;
    }

    public boolean isLeased() {
        return this.lease != null;
    }

    @Override
    public String toString() {
        return String.format("{ hybridId: %s, book: %s, availability: %s }",
                this.getHybridId(),
                this.getBook().toString(),
                isNull(this.getLease()) ? "available" : "leased");
    }

    public String getHybridId() {
        return hybridId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Lease getLease() {
        return lease;
    }

    public void setLease(Lease lease) {
        this.lease = lease;
    }

    public static class BookCopyBuilder extends BaseBuilder<BookCopy, BookCopyBuilder> {

        private String hybridId;

        private Book book;

        private Lease lease;

        public BookCopyBuilder() {
        }

        public BookCopyBuilder hybridId(String hybridId) {
            this.hybridId = hybridId;
            return this;
        }

        public BookCopyBuilder book(Book book) {
            this.book = book;
            return this;
        }

        public BookCopyBuilder lease(Lease lease) {
            this.lease = lease;
            return this;
        }

        @Override
        public BookCopy build() {
            BookCopy bookCopy = new BookCopy(this);
            return bookCopy;
        }
    }
}
