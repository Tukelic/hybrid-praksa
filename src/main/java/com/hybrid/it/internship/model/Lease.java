package com.hybrid.it.internship.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Lease extends BaseEntity {

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "lease")
    private Set<BookCopy> bookCopies;

    @Column(nullable = false)
    private LocalDateTime leaseDateTime;

    @Column(nullable = true)
    private LocalDateTime returnDateTime;

    public Lease() {
        this.leaseDateTime = LocalDateTime.now();
    }

    public Lease(User user) {
        this.user = user;
        this.leaseDateTime = LocalDateTime.now();
    }

    public Lease(User user, Set<BookCopy> bookCopies) {
        this.user = user;
        this.bookCopies = bookCopies;
        this.leaseDateTime = LocalDateTime.now();
    }

    public boolean isOverdue(long amount, TemporalUnit unit) {
        return leaseDateTime.plus(amount, unit).isBefore(LocalDateTime.now());
    }

    public void addBookCopy(BookCopy bookCopy) {
        if (bookCopies == null) {
            bookCopies = new HashSet<>();
        }
        bookCopies.add(bookCopy);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(Set<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public LocalDateTime getLeaseDateTime() {
        return leaseDateTime;
    }

    public void setLeaseDateTime(LocalDateTime leaseDateTime) {
        this.leaseDateTime = leaseDateTime;
    }

    public LocalDateTime getReturnDateTime() {
        return returnDateTime;
    }

    public void setReturnDateTime(LocalDateTime returnDateTime) {
        this.returnDateTime = returnDateTime;
    }
}
