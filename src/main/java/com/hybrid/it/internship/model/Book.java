package com.hybrid.it.internship.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Book extends BaseEntity {

    @Column(updatable = false)
    private LocalDateTime creationDateTime;

    private String title;

    private String author;

    @Column(unique = true)
    private String isbn;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "book")
    private Set<BookCopy> bookCopies;

    protected Book() {
        super();
    }

    protected Book(BookBuilder bookBuilder) {
        super(bookBuilder.id);
        this.creationDateTime = LocalDateTime.now();
        this.title = bookBuilder.title;
        this.author = bookBuilder.author;
        this.isbn = bookBuilder.isbn;
        this.bookCopies = bookBuilder.bookCopies;
    }

    public static BookBuilder builder() {
        return new BookBuilder();
    }

    @Override
    public String toString() {
        return String.format("{ title: %s, author: %s, ISBN: %s }", this.getTitle(), this.getAuthor(), this.getIsbn());
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public Set<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public static class BookBuilder extends BaseBuilder<Book, BookBuilder> {

        private LocalDateTime creationDateTime;

        private String title;

        private String author;

        private String isbn;

        private Set<BookCopy> bookCopies;

        public BookBuilder() {
        }

        public BookBuilder creationDateTime(LocalDateTime creationDateTime) {
            this.creationDateTime = creationDateTime;
            return this;
        }

        public BookBuilder title(String title) {
            this.title = title;
            return this;
        }

        public BookBuilder author(String author) {
            this.author = author;
            return this;
        }

        public BookBuilder isbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public BookBuilder bookCopies(Set<BookCopy> bookCopies) {
            this.bookCopies = bookCopies;
            return this;
        }

        @Override
        public Book build() {
            Book book = new Book(this);
            return book;
        }
    }

}
