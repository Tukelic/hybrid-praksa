package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.dto.LeaseDTO;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.model.Lease;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class LeaseConverterServiceImpl implements ConverterService<LeaseDTO, Lease> {

    private final ConverterService<BookCopyDTO, BookCopy> bookCopyConverter;

    public LeaseConverterServiceImpl(ConverterService<BookCopyDTO, BookCopy> bookCopyConverter) {
        this.bookCopyConverter = bookCopyConverter;
    }

    @Override
    public LeaseDTO convertToDto(Lease lease) {
        Set<BookCopyDTO> bookCopyDTOS = lease.getBookCopies()
                .stream().map(bookCopyConverter::convertToDto).collect(Collectors.toSet());
        return new LeaseDTO(lease.getId(), bookCopyDTOS, lease.getLeaseDateTime());
    }

    @Override
    public Lease convertToEntity(LeaseDTO leaseDTO) {
        return null;
    }
}
