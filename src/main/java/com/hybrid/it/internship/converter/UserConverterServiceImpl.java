package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.UserDTO;
import com.hybrid.it.internship.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverterServiceImpl implements ConverterService<UserDTO, User> {

    @Override
    public UserDTO convertToDto(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
    }

    @Override
    public User convertToEntity(UserDTO userDTO) {
        return User.builder()
                .id(userDTO.getId())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .email(userDTO.getEmail())
                .build();
    }
}
