package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.dto.BookDTO;
import com.hybrid.it.internship.model.Book;
import com.hybrid.it.internship.model.BookCopy;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class BookConverterServiceImpl implements ConverterService<BookDTO, Book> {

    private final ConverterService<BookCopyDTO, BookCopy> bookCopyConverter;

    public BookConverterServiceImpl(ConverterService<BookCopyDTO, BookCopy> bookCopyConverter) {
        this.bookCopyConverter = bookCopyConverter;
    }

    @Override
    public BookDTO convertToDto(Book book) {
        return BookDTO.builder()
                .id(book.getId())
                .creationDateTime(book.getCreationDateTime())
                .title(book.getTitle())
                .author(book.getAuthor())
                .isbn(book.getIsbn())
                .bookCopies(book.getBookCopies()
                        .stream()
                        .map(bookCopyConverter::convertToDto)
                        .collect(Collectors.toSet()))
                .build();
    }

    @Override
    public Book convertToEntity(BookDTO bookDTO) {
        return Book.builder()
                .id(bookDTO.getId())
                .title(bookDTO.getTitle())
                .author(bookDTO.getAuthor())
                .isbn(bookDTO.getIsbn())
                .bookCopies(bookDTO.getBookCopies()
                        .stream()
                        .map(bookCopyConverter::convertToEntity)
                        .collect(Collectors.toSet()))
                .build();
    }
}
