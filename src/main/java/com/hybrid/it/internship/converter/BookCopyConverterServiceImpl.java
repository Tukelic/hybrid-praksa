package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BookCopyDTO;
import com.hybrid.it.internship.model.BookCopy;
import com.hybrid.it.internship.service.BookService;
import org.springframework.stereotype.Component;

@Component
public class BookCopyConverterServiceImpl implements ConverterService<BookCopyDTO, BookCopy> {

    private final BookService bookService;

    public BookCopyConverterServiceImpl(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public BookCopyDTO convertToDto(BookCopy bookCopy) {
        return BookCopyDTO.builder()
                .id(bookCopy.getId())
                .hybridId(bookCopy.getHybridId())
                .bookId(bookCopy.getBook().getId())
                .build();
    }

    @Override
    public BookCopy convertToEntity(BookCopyDTO bookCopyDTO) {
        return BookCopy.builder()
                .id(bookCopyDTO.getId())
                .hybridId(bookCopyDTO.getHybridId())
                .book(bookService.findById(bookCopyDTO.getBookId()))
                .build();
    }
}
