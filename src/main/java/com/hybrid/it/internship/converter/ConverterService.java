package com.hybrid.it.internship.converter;

import com.hybrid.it.internship.dto.BaseDTO;
import com.hybrid.it.internship.model.BaseEntity;

public interface ConverterService<DTO extends BaseDTO, ENTITY extends BaseEntity> {

    DTO convertToDto(ENTITY entity);

    ENTITY convertToEntity(DTO dto);
}
