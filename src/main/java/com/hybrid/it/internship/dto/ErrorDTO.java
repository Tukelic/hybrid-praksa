package com.hybrid.it.internship.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ErrorDTO {

    private String message;

    private int internalStatusCode;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SSS")
    private LocalDateTime timestamp;

    private List<String> fieldErrors = new ArrayList<>();

    protected ErrorDTO() {
    }

    protected ErrorDTO(ErrorDTOBuilder builder) {
        this.message = builder.message;
        this.internalStatusCode = builder.internalStatusCode;
        this.timestamp = builder.timestamp;
        this.fieldErrors = builder.fieldErrors;
    }

    public static ErrorDTOBuilder builder() {
        return new ErrorDTOBuilder();
    }

    public String getMessage() {
        return message;
    }

    public int getInternalStatusCode() {
        return internalStatusCode;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<String> getFieldErrors() {
        return fieldErrors;
    }

    public static class ErrorDTOBuilder {

        private String message;

        private int internalStatusCode;

        private LocalDateTime timestamp;

        private List<String> fieldErrors = new ArrayList<>();

        public ErrorDTOBuilder() {
        }

        public ErrorDTOBuilder message(String message) {
            this.message = message;
            return this;
        }

        public ErrorDTOBuilder internalStatusCode(int internalStatusCode) {
            this.internalStatusCode = internalStatusCode;
            return this;
        }

        public ErrorDTOBuilder timestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public ErrorDTOBuilder fieldErrors(List<String> fieldErrors) {
            this.fieldErrors = fieldErrors;
            return this;
        }

        public ErrorDTO build() {
            ErrorDTO errorDTO = new ErrorDTO(this);
            return errorDTO;
        }
    }
}
