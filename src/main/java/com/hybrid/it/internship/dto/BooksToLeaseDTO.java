package com.hybrid.it.internship.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

public class BooksToLeaseDTO {

    @Size(min = 1, max = 3)
    private List<Long> bookIds;

    @NotNull
    @Positive
    private Long userId;

    public BooksToLeaseDTO() {
    }

    public BooksToLeaseDTO(List<Long> bookIds, Long userId) {
        this.bookIds = bookIds;
        this.userId = userId;
    }

    public List<Long> getBookIds() {
        return bookIds;
    }

    public void setBookIds(List<Long> bookIds) {
        this.bookIds = bookIds;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
