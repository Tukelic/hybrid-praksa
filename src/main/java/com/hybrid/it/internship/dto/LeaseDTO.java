package com.hybrid.it.internship.dto;

import java.time.LocalDateTime;
import java.util.Set;

public class LeaseDTO implements BaseDTO {

    private Long id;

    private Set<BookCopyDTO> bookCopyDTOs;

    private LocalDateTime leaseDateTime;

    public LeaseDTO() {
    }

    public LeaseDTO(Long id, Set<BookCopyDTO> bookCopyDTOs, LocalDateTime leaseDateTime) {
        this.id = id;
        this.bookCopyDTOs = bookCopyDTOs;
        this.leaseDateTime = leaseDateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<BookCopyDTO> getBookCopyDTOs() {
        return bookCopyDTOs;
    }

    public void setBookCopyDTOs(Set<BookCopyDTO> bookCopyDTOs) {
        this.bookCopyDTOs = bookCopyDTOs;
    }

    public LocalDateTime getLeaseDateTime() {
        return leaseDateTime;
    }

    public void setLeaseDateTime(LocalDateTime leaseDateTime) {
        this.leaseDateTime = leaseDateTime;
    }
}
