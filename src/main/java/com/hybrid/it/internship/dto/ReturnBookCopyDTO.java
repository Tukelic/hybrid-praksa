package com.hybrid.it.internship.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ReturnBookCopyDTO {

    @NotNull
    @Positive
    private Long bookCopyId;

    public ReturnBookCopyDTO() {
    }

    public ReturnBookCopyDTO(Long bookCopyId) {
        this.bookCopyId = bookCopyId;
    }

    public Long getBookCopyId() {
        return bookCopyId;
    }

    public void setBookCopyId(Long bookCopyId) {
        this.bookCopyId = bookCopyId;
    }
}
