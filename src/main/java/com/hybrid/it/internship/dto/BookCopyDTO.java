package com.hybrid.it.internship.dto;

public class BookCopyDTO implements BaseDTO {

    private Long id;

    private String hybridId;

    private Long bookId;

    protected BookCopyDTO() {
    }

    protected BookCopyDTO(BookCopyDTOBuilder bookCopyDTOBuilder) {
        this.id = bookCopyDTOBuilder.id;
        this.hybridId = bookCopyDTOBuilder.hybridId;
        this.bookId = bookCopyDTOBuilder.bookId;
    }

    public static BookCopyDTOBuilder builder() {
        return new BookCopyDTOBuilder();
    }

    public Long getId() {
        return id;
    }

    public String getHybridId() {
        return hybridId;
    }

    public Long getBookId() {
        return bookId;
    }

    public static class BookCopyDTOBuilder {

        private Long id;

        private String hybridId;

        private Long bookId;

        public BookCopyDTOBuilder() {
        }

        public BookCopyDTOBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public BookCopyDTOBuilder hybridId(String hybridId) {
            this.hybridId = hybridId;
            return this;
        }

        public BookCopyDTOBuilder bookId(Long bookId) {
            this.bookId = bookId;
            return this;
        }

        public BookCopyDTO build() {
            BookCopyDTO bookCopyDTO = new BookCopyDTO(this);
            return bookCopyDTO;
        }
    }
}
