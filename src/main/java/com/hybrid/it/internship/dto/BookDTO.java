package com.hybrid.it.internship.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIgnoreProperties(value = {"creationDateTime"}, allowGetters = true)
public class BookDTO implements BaseDTO {

    private Long id;

    private LocalDateTime creationDateTime;

    @NotBlank(message = "Book must have a title")
    private String title;

    @NotBlank(message = "Book must have an author")
    @Pattern(message = "Author must have alphanumeric characters", regexp = "^[A-Za-z]+\\s[A-Za-z]*$")
    private String author;

    @Pattern(message = "ISBN must be in this format: 123-4-56-789012-3", regexp = "^[0-9]{3}-[0-9]-[0-9]{2}-[0-9]{6}-[0-9]$")
    private String isbn;

    @NotNull
    private Set<BookCopyDTO> bookCopies;

    protected BookDTO() {
    }

    protected BookDTO(BookDTOBuilder bookDTOBuilder) {
        this.id = bookDTOBuilder.id;
        this.creationDateTime = bookDTOBuilder.creationDateTime;
        this.title = bookDTOBuilder.title;
        this.author = bookDTOBuilder.author;
        this.isbn = bookDTOBuilder.isbn;
        this.bookCopies = bookDTOBuilder.bookCopies;
    }

    public static BookDTOBuilder builder() {
        return new BookDTOBuilder();
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public Set<BookCopyDTO> getBookCopies() {
        return bookCopies;
    }

    public static class BookDTOBuilder {

        private Long id;

        private LocalDateTime creationDateTime;

        private String title;

        private String author;

        private String isbn;

        private Set<BookCopyDTO> bookCopies;

        public BookDTOBuilder() {
        }

        public BookDTOBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public BookDTOBuilder creationDateTime(LocalDateTime creationDateTime) {
            this.creationDateTime = creationDateTime;
            return this;
        }

        public BookDTOBuilder title(String title) {
            this.title = title;
            return this;
        }

        public BookDTOBuilder author(String author) {
            this.author = author;
            return this;
        }

        public BookDTOBuilder isbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public BookDTOBuilder bookCopies(Set<BookCopyDTO> bookCopies) {
            this.bookCopies = bookCopies;
            return this;
        }

        public BookDTO build() {
            BookDTO bookDTO = new BookDTO(this);
            return bookDTO;
        }
    }
}