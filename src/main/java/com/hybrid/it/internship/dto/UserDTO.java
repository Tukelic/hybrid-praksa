package com.hybrid.it.internship.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO implements BaseDTO {

    private Long id;

    @Size(min = 3, message = "Username must contain at least 3 characters")
    @Pattern(regexp = "^\\S*$")
    private String username;

    @Size(min = 5, message = "Password must contain at least 5 characters")
    private String password;

    @NotBlank(message = "First name cannot be blank")
    @Pattern(message = "First name must have alphanumeric characters", regexp = "^[A-Za-z]+$")
    private String firstName;

    @NotBlank(message = "Last name cannot be blank")
    @Pattern(message = "Last name must have alphanumeric characters", regexp = "^[A-Za-z]+$")
    private String lastName;

    @Email(message = "Email not valid")
    private String email;

    protected UserDTO() {
    }

    protected UserDTO(UserDTOBuilder userDTOBuilder) {
        this.id = userDTOBuilder.id;
        this.username = userDTOBuilder.username;
        this.password = userDTOBuilder.password;
        this.firstName = userDTOBuilder.firstName;
        this.lastName = userDTOBuilder.lastName;
        this.email = userDTOBuilder.email;
    }

    public static UserDTOBuilder builder() {
        return new UserDTOBuilder();
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public static class UserDTOBuilder {

        private Long id;

        private String username;

        private String password;

        private String firstName;

        private String lastName;

        private String email ;

        public UserDTOBuilder() {
        }

        public UserDTOBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public UserDTOBuilder username(String username) {
            this.username = username;
            return this;
        }

        public UserDTOBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserDTOBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserDTOBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserDTOBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserDTO build() {
            UserDTO userDTO = new UserDTO(this);
            return userDTO;
        }
    }
}
